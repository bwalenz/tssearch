import tssearch

from random import randint

#create some random binary strings
strings = []
# how many strings to generate
n = 50
# length of each string
l = 512
# size of the wavelet window, power of 2 please.
w = 32
# number of wavelet features to use in index
f = 3

for i in range(0, n):
    bstr = []
    for b in range(0, l):
        bstr.append(randint(0, 1))
    strings.append(bstr)


index = tssearch.FIndex(f=f, w=w, l=l, transform='haar')
index.insert(strings)

query = strings[0]
objs = index.intersection(query)
print("Query objects: {0}".format(len(objs)))
