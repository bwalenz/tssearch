.. _class:

Class Documentation
--------------------------------------------------------------------

.. autoclass:: tssearch.findex.FIndex
    :members: __init__, intersection, nearest, prefixes

.. autoclass:: tssearch.mbr.MBR
    :members: __init__, marginal_cost, append, dimensions

.. autoclass:: tssearch.mbr.MBRScale
    :members: __init__, scale

