import os
from setuptools import setup, find_packages

setup(
    name = "tssearch",
    version = "1.0",
    author = "Brett Walenz",
    author_email = "bwalenz@cs.duke.edu",
    description = ("Time Series Similarity Index and Search"),
    license = "BSD",
    packages = find_packages(),
)
