from rtree import index
import numpy as np

from . import mbr
from . import haar


class FIndex(object):
    """ An F-Index implementation based on Faloutsos et. al.

    Faloutsos, Christos, Mudumbai Ranganathan, and Yannis Manolopoulos. Fast subsequence matching in time-series databases. Vol. 23. No. 2. ACM, 1994.

    """
    def __init__(self, f=3, w=32, l=512, transform='haar'):
        """ Creates a new f-index based on a specific transform. 

        :param f:
            The number of coefficients to use from the transform for each point. 

        :param w:
            Size of the sliding window. Creates l-w+1 transformed subsequences.

        :param l: 
            Length of the strings. Currently does not handle varying-length strings.
            
        :param transform:
            'haar' or 'dft'. Haar transform or discrete fourier transform, respectively.
        """ 
        self.f = f
        self.w = w
        self.l = l
        p = index.Property()
        p.dimension = f
        self.size = 0
        if transform == 'haar':
            self.transform = haar.haar_transform 
        else:
            self.transform = np.fft.fft
        self.idx = index.Index('sim', pagesize=4096, properties=p, interleaved=False)

    def intersection(self, q, regions=False):
        """ Calculate the intersection of query sequence :attr:'q' with the FIndex. 

        :param q:
            The query subsequence. If q is longer than w, q is partitioned into |q|/w partitions
            with each partition being searched. 

        :param regions: True or False, defaults to False
            If regions is False, returns only the leaf-level objects in the index. If true, returns every 
            constructed region in the index. 
        """

        qparts = self.prefixes(q)
        objects = []
        #TODO: we don't do any duplicate handling at this point
        for part in qparts:
            point = [x for pair in zip(part, part) for x in pair]
            objs = list(self.idx.intersection(point, objects=True))
            if not regions:
                for obj in objs:
                    if obj.object:
                        objects.append(obj.object)
            else:
                objects.extend(objs)
        return objects

    def nearest(self, q, k):
        """ Find the k-nearest neighbors to q. 

        :param q:
            The query subsequence. If q is longer than w, q is partitioned into |q|/w partitions
            with each partition being searched. 

        :param k:
            Number of nearest neighbors.
        """

        qparts = self.prefixes(q)
        #TODO: we need to rerank the candidates and return k of them
        candidates = []
        for part in qparts:
            objs = self.idx.nearest(part, k, objects=True)
            candidates.append(objs)
        return candidates 

    def prefixes(self, q):
        """Split apart q into |q|/w prefix strings, potentially leaving off the last w-1 values
        if the last prefix is too short."""

        #TODO: handle or throw exceptions for small queries
        ps = []
        if len(q) == self.w:
            ps.append(self.transform(q)[0:self.f])
        elif len(q) > self.w:
            ps = [self.transform(q[0:self.w])[0:self.f]]
            idx = self.w
            while idx + self.w < len(q):
                part = q[idx:idx+self.w]
                if len(part) < self.w:
                    break
                ps.append(self.transform(part)[0:self.f])
                idx = idx + self.w
        return ps 

    def insert(self, objs):
        """Insert a collection of objects into the findex. 
        See: section 3 of Faloutsos et. al.

        Algorithm sketch:

        For each object o:
            for each window w in o[i:i+w]
                transform(o[i:i+w])
                store f coefficients
                save min and max coefficient for each i in f
            add transforms to collection

        for each collection of transforms:
            calculate minimum bounding rectangle by calculating the minimum cost
            if minimum cost increases, create a new minimum bounding rectangle
        
        for all the mbr's, insert into the index
        """

        forms = []
        num_windows = self.l - self.w + 1
        maximums = [0 for i in range(0, self.f)] #maximum values of each dimension, used for scaling
        minimums = [0 for i in range(0, self.f)] #min values of each dimension, used for shifting before scaling 
        for s in objs:
            subforms = []
            for i in range(0, num_windows):
                h = self.transform(s[i:i+self.w])[0:self.f]
                for i, v in enumerate(h):
                    if v > maximums[i]:
                        maximums[i] = v
                    if v < minimums[i]:
                        minimums[i] = v
                subforms.append(h)
            forms.append(subforms)

        self.scale = mbr.MBRScale(maximums=maximums, minimums=minimums)
        #now calculate the mbrs for each transform 
        mbrs = []
        for w in forms:
            curr_mbr = mbr.MBR(self.scale, f=self.f)
            for s in w:
                #for each subwave, see if the marginal cost exceeds the current marginal
                if not curr_mbr.objects:
                    curr_mbr.append(s)
                else: 
                    mc = curr_mbr.marginal_cost()
                    pmc = curr_mbr.marginal_cost(s)
                    #print("marginal cost {0}".format(mc))
                    #print("proposed marginal cost {0}".format(pmc))
                    if pmc <= mc:
                        curr_mbr.append(s)
                    else:
                        mbrs.append(curr_mbr)
                        curr_mbr = mbr.MBR(self.scale, f=self.f)
                        curr_mbr.append(s)
            mbrs.append(curr_mbr)
        print("MBRs[0-10]: {0}".format(mbrs[0:10]))
        print("Number of mbrs {0}".format(len(mbrs)))

        for m in mbrs:
            self.size += 1
            self.idx.insert(self.size, m.dimensions(), obj=m)

