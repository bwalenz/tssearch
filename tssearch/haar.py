import math
import numpy as np

def haar_transform(array):
    return np.array(recursive_haar(array))

def recursive_haar(haar_array):
    first = []
    second = []
    if len(haar_array) < 2:
        return haar_array
    for i in range(0, len(haar_array), 2):
        a = haar_array[i]
        b = haar_array[i+1]
        first.append((a + b) / math.sqrt(2))
        second.append((a - b) / math.sqrt(2))
    first = recursive_haar(first)
    return first + second



