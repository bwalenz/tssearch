import numpy as np

class MBR(object):
    def __init__(self, scale, f=3):
        self.objects = []
        self.extents = []
        self.scaled_extents = []
        self.f = f
        self.scale = scale

    def marginal_cost(self, proposed_item=np.array([])):
        cost = 0
        if proposed_item.any():
            scaled_proposed = self.scale.scale(proposed_item)
        for index, bounds in enumerate(self.extents):
            slow, shigh = self.scale.scale(bounds)
            
            if proposed_item.any():
                sitem = scaled_proposed[index]
                if sitem < slow:
                    slow = sitem 
                if sitem > shigh:
                    shigh = sitem
            if cost == 0:
                cost = slow + shigh + .5
            else: 
                cost *= slow + shigh + .5
        k = len(self.objects)
        if proposed_item.any():
            k += 1
        marginal_cost = cost / k
        return marginal_cost

    def append(self, obj):
        if not self.objects:
            for i in range(0, self.f):
                self.extents.append((obj[i], obj[i]))
        else: 
            for i, value in enumerate(obj):
                low, high = self.extents[i]
                if value < low:
                    low = value
                if value > high:
                    high = value
                self.extents[i] = (low, high) 

        self.objects.append(obj)

    def __repr__(self):
        return "Objects: {0}, Extents {1}".format(len(self.objects), self.extents)

    def dimensions(self):
        dimensions = []
        for low, high in self.extents:
            dimensions.append(low)
            dimensions.append(high)
        return dimensions
        

class MBRScale(object):
    def __init__(self, maximums=[], minimums=[]):
        self.maximums = maximums
        self.minimums = minimums

    def scale(self, arr):
        scaled = []
        for i, value in enumerate(arr):
            scaled.append((value + self.minimums[i] * - 1) / self.maximums[i])
        return scaled


